# daverona/docker-compose/postgres

## Quick Start

```bash
cp .env.example .env  
# edit .env
```

If you want to initialize your instance with certain files (with extension `.sh`, `.sql`, and `.sql.gz`) 
when a container runs for the *first time*, copy files to `storage/postgres/initdb.d` directory.

Run a container:

```bash
docker-compose up --detach
```

Log in to postgres:

```bash
docker-compose exec postgres psql --username=postgres
```

## References

* PostgreSQL Documentation: [https://www.postgresql.org/docs/](https://www.postgresql.org/docs/)
* Server Configuration: [https://www.postgresql.org/docs/9.6/runtime-config.html](https://www.postgresql.org/docs/9.6/runtime-config.html)
* PostgreSQL repository: [https://www.postgresql.org/ftp/source/](https://www.postgresql.org/ftp/source/)
* PostgreSQL registry: [https://hub.docker.com/\_/postgres](https://hub.docker.com/_/postgres)
